const modelSchema = require("../models/auditlog");
require("dotenv/config");

function getSortOrder(prop) {
  return function (a, b) {
    if (a[prop] > b[prop]) {
      return 1;
    } else if (a[prop] < b[prop]) {
      return -1;
    }
    return 0;
  };
}

async function insertAuditLog(
  objectId,
  ip,
  message,
  action,
  type,
  before,
  after,
  createdUserId
) {
  try {
    if (process.env.NODE_ENV === "test") {
      return { result: "OK" };
    }
    const newItem = new modelSchema({
      objectId: objectId,
      ip: ip,
      message: message,
      action: action,
      type: type,
      before: before,
      after: after,
      createdUserId: createdUserId,
      createdDate: new Date(),
    });
    await newItem.save();
    return { result: "OK" };
  } catch (err) {
    return { result: "ERROR", message: err.message };
  }
}

exports.getSortOrder = getSortOrder;
exports.insertAuditLog = insertAuditLog;
