const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const connection = require("./repository/connection");
require("dotenv/config");

app.use(bodyParser.json({ limit: "30000kb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30000kb", extended: true }));

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PATCH,DELETE");
  next();
});

const orderRoute = require("./routes/order");
app.use(process.env.API_ORDER_URL, orderRoute.router);

const auditLogRoute = require("./routes/auditlog");
app.use(process.env.API_AUDIT_URL, auditLogRoute);

connection.connect();

if (!module.parent) {
  app.listen(process.env.PORT, () =>
    console.log(`Example app listening on port ${process.env.PORT}!`)
  );
}

module.exports = app;
