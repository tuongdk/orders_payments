const mongoose = require("mongoose");
require("dotenv/config");

function connect() {
  if (process.env.NODE_ENV === "test") {
    var Mockgoose = require('mockgoose').Mockgoose;
    var mockgoose = new Mockgoose(mongoose);
    mockgoose.prepareStorage().then(function () {
      mongoose
        .connect(
          process.env.DB_CONNECTION,
          {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useFindAndModify: false,
          },
          () => {
            console.log("Contected to DB test!");
          }
        )
        .catch((err) => {
          console.log("DB Connection test Error: " + err);
        });
    });
  } else {
    mongoose
      .connect(
        process.env.DB_CONNECTION,
        {
          useUnifiedTopology: true,
          useNewUrlParser: true,
          useFindAndModify: false,
        },
        () => {
          console.log("Contected to DB!");
        }
      )
      .catch((err) => {
        console.log("DB Connection Error: " + err);
      });
  }
}

function close() {
  mongoose.disconnect()
}

module.exports = { connect, close }