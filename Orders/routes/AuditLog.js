const express = require("express");
const modelSchema = require("../models/auditlog");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const items = await modelSchema.find().sort("-createdDate");
    res.json({ result: "OK", totalCount: items.length, data: items });
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

router.get("/:id", async (req, res) => {
  try {
    item = await modelSchema.findById(req.params.id);
    res.json({ result: "OK", data: item });
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

module.exports = router;