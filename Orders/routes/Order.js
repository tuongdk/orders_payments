const express = require("express");
const modelSchema = require("../models/order");
const router = express.Router();
const utilities = require("../utilities/index");
const axios = require("axios");

router.get("/", async (req, res) => {
  try {
    const items = await modelSchema.find().sort("-purchasedDate");
    res.json({ result: "OK", totalCount: items.length, data: items });
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

router.get("/:id", async (req, res) => {
  try {
    if (
      req.params.id === undefined ||
      req.params.id === null ||
      req.params.id === ""
    ) {
      return res.json({
        result: "ERROR",
        message: process.env.MSG_WRONG_PARAMS,
      });
    }
    const item = await modelSchema.findById(req.params.id);
    if (item) {
      return res.json({ result: "OK", data: item });
    }
    return res.json({ result: "ERROR", message: process.env.MSG_NOT_EXIST });
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

router.get("/checkstatus/:orderNumber", async (req, res) => {
  try {
    if (
      req.params.orderNumber === undefined ||
      req.params.orderNumber === null ||
      req.params.orderNumber === ""
    ) {
      return res.json({
        result: "ERROR",
        message: process.env.MSG_WRONG_PARAMS,
      });
    }
    const item = await modelSchema.findOne({
      orderNumber: req.params.orderNumber,
    });
    if (item) {
      return res.json({ result: "OK", status: item.status });
    }
    return res.json({ result: "ERROR", message: process.env.MSG_NOT_EXIST });
  } catch (err) {
    res.json({ result: "ERROR 1", message: err.message });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    await modelSchema.findOneAndDelete(
      { _id: req.params.id },
      async function callBack(err, deletedDoc) {
        if (err) {
          return res.json({ result: "ERROR", message: err.message });
        }
        if (deletedDoc) {
          //insert audit log
          insertAuditLog(
            deletedDoc,
            JSON.stringify(parseLogObject(deletedDoc)),
            "",
            "Delete",
            req.requestUserId
          );
          return res.json({
            result: "OK",
            message: process.env.MSG_DELETE_SUCCESS,
          });
        }
        return res.json({
          result: "ERROR",
          message: process.env.MSG_NOT_EXIST_DELETE,
        });
      }
    );
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

router.patch("/cancelorder/:orderNumber", async (req, res) => {
  try {
    if (
      req.params.orderNumber === undefined ||
      req.params.orderNumber === null ||
      req.params.orderNumber === ""
    ) {
      return res.json({
        result: "ERROR",
        message: process.env.MSG_WRONG_PARAMS,
      });
    }

    let order = await modelSchema.findOne({
      orderNumber: req.params.orderNumber,
    });
    if (order === undefined || order === null) {
      return res.json({
        result: "ERROR",
        message: process.env.MSG_NOT_EXIST_UPDATE,
      });
    }

    const requestUserId = req.body.requestUserId;
    const status = "Cancelled";

    if (order) {
      if (status === "Cancelled") {
        //Order only allow cancel if status in (Created, Confirmed)
        if (order.status === "Cancelled") {
          return { result: "ERROR", message: process.env.MSG_ORDER_CANCELLED };
        }
        if (order.status === "Delivered") {
          return { result: "ERROR", message: process.env.MSG_ORDER_DELIVERED };
        }
      } else if (status === "Confirmed") {
        //Order only allow confirm if status is Created
        if (order.status !== "Created") {
          return {
            result: "ERROR",
            message: process.env.MSG_ORDER_INVALID_CONFIRMED,
          };
        }
      }
    }
    await modelSchema.findOneAndUpdate(
      { _id: order._id },
      {
        $set: {
          status: status,
          modifiedUserId: requestUserId,
          modifiedDate: new Date(),
        },
      },
      {
        new: true,
      },
      (err, updatedItem) => {
        if (err) {
          return { result: "ERROR", message: error.message };
        }
        if (updatedItem) {
          // insert audit log
          insertAuditLog(
            updatedItem,
            JSON.stringify(parseLogObject(order)),
            JSON.stringify(parseLogObject(updatedItem)),
            "Update",
            requestUserId
          );
          return { result: "OK", message: process.env.MSG_CANCEL_SUCCESS };
        }
        return { result: "ERROR", message: process.env.MSG_NOT_EXIST_UPDATE };
      }
    );
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

function validate(obj) {
  if (
    obj.customerName === undefined ||
    obj.customerName === null ||
    obj.customerName.trim() === ""
  ) {
    return {
      result: "ERROR",
      message: "Customer name is required.",
    };
  }
  if (
    obj.shipToName === undefined ||
    obj.shipToName === null ||
    obj.shipToName.trim() === ""
  ) {
    return {
      result: "ERROR",
      message: "Ship to name is required.",
    };
  }
  if (
    obj.shipToAddress === undefined ||
    obj.shipToAddress === null ||
    obj.shipToAddress.trim() === ""
  ) {
    return {
      result: "ERROR",
      message: "Ship to address is required.",
    };
  }
  if (
    obj.totalAmount === undefined ||
    obj.totalAmount === null ||
    obj.totalAmount <= 0
  ) {
    return {
      result: "ERROR",
      message: "Total amount must be greater than zero.",
    };
  }
  if (
    obj.requestUserId === undefined ||
    obj.requestUserId === null ||
    obj.requestUserId === ""
  ) {
    return {
      result: "ERROR",
      message: "Missing parameter requestUserId.",
    };
  }
  return {
    result: "OK",
    message: "Valid",
  };
}

router.post("/", async (req, res) => {
  try {
    const resultValidation = validate(req.body);
    if (resultValidation.result !== "OK") {
      return res.json(resultValidation);
    }

    const currentNumber = (await modelSchema.countDocuments()) + 1;
    let orderNumber = "ORDER-" + ("0000" + currentNumber).slice(-5);
    const newItem = new modelSchema({
      status: "Created",
      orderNumber: orderNumber,
      customerName: req.body.customerName,
      shipToName: req.body.shipToName,
      shipToAddress: req.body.shipToAddress,
      totalAmount: req.body.totalAmount,
      createdUserId: req.body.requestUserId,
      createdDate: new Date(),
      modifiedUserId: null,
      modifiedDate: null,
    });
    await newItem.save(async (err, doc) => {
      if (err) {
        return res.json({ result: "ERROR", message: err.message });
      }
      //invoke payment app for payment
      await callPayment(newItem, req.body.requestUserId);

      //insert audit log
      insertAuditLog(
        newItem,
        JSON.stringify(parseLogObject(newItem)),
        "",
        "Create",
        req.createdUserId
      );
      return res.json({
        result: "OK",
        message: process.env.MSG_CREATE_SUCCESS,
      });
    });
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

async function callPayment(order, requestUserId) {
  axios
    .post(process.env.PAYMENT_URL + "/payment", {
      token: process.env.PAYMENT_TOKEN,
      payment: {
        amount: order.totalAmount,
        shipToName: order.shipToName,
        shipToAddress: order.shipToAddress,
      },
    })
    .then(async (result) => {
      if (result.status !== 200) {
        console.log(
          "Payment request for order with ID " +
            order._id +
            " failed as response status not 200."
        );
        return;
      }
      if (result.data === undefined || result.data === null) {
        console.log(
          "Payment request for order with ID " +
            order._id +
            " failed as response data is undefined."
        );
        return;
      }
      if (result.data.result !== "OK") {
        console.log(
          "Payment request for order with ID " +
            order._id +
            " failed with error: " +
            result.data.message
        );
        return;
      }
      //Request success
      if (result.data.responseCode === "Declined") {
        //Update order status
        await updateOrderStatus(order, "Cancelled", requestUserId);
      } else if (result.data.responseCode === "Confirmed") {
        //Update order status
        await updateOrderStatus(order, "Confirmed", requestUserId).then(
          async (res) => {
            //After X amount of seconds confirmed orders should automatically be moved to the delivered state.
            setTimeout(async () => {
              await updateOrderStatus(order, "Delivered", requestUserId);
            }, process.env.PAYMENT_TIMER_SECOND * 1000);
          }
        );
      }
    })
    .catch((e) => {
      console.log(e.message);
    });
}

async function updateOrderStatus(order, status, requestUserId) {
  try {
    if (
      order === undefined ||
      order === null ||
      status === undefined ||
      status === "" ||
      requestUserId === undefined ||
      requestUserId === ""
    ) {
      return { result: "ERROR", message: process.env.MSG_WRONG_PARAMS };
    }

    if (order) {
      if (status === "Cancelled") {
        //Order only allow cancel if status in (Created, Confirmed)
        if (order.status === "Cancelled") {
          return { result: "ERROR", message: process.env.MSG_ORDER_CANCELLED };
        }
        if (order.status === "Delivered") {
          return { result: "ERROR", message: process.env.MSG_ORDER_DELIVERED };
        }
      } else if (status === "Confirmed") {
        //Order only allow confirm if status is Created
        if (order.status !== "Created") {
          return {
            result: "ERROR",
            message: process.env.MSG_ORDER_INVALID_CONFIRMED,
          };
        }
      }
    }
    await modelSchema.findOneAndUpdate(
      { _id: order._id },
      {
        $set: {
          status: status,
          modifiedUserId: requestUserId,
          modifiedDate: new Date(),
        },
      },
      {
        new: true,
      },
      (err, updatedItem) => {
        if (err) {
          return { result: "ERROR", message: error.message };
        }
        if (updatedItem) {
          //insert audit log
          insertAuditLog(
            updatedItem,
            JSON.stringify(parseLogObject(order)),
            JSON.stringify(parseLogObject(updatedItem)),
            "Update",
            requestUserId
          );
          return { result: "OK", message: process.env.MSG_CANCEL_SUCCESS };
        }
        return { result: "ERROR", message: process.env.MSG_NOT_EXIST_UPDATE };
      }
    );
  } catch (err) {
    return { result: "ERROR", message: err.message };
  }
}

//Using for audit logs

//This JSON to be displayed on audit log detail form
function parseLogObject(doc) {
  return {
    Status: doc.status,
    "Order Number": doc.orderNumber,
    "Purchased Date": doc.purchasedDate,
    "Customer Name": doc.customerName,
    "Ship To Name": doc.shipToName,
    "Ship To Address": doc.shipToAddress,
    "Total Amount": doc.totalAmount,
  };
}

async function insertAuditLog(doc, before, after, action, createdUserId) {
  utilities.insertAuditLog(
    doc._id.toString(),
    action + " order",
    action,
    "Order",
    before,
    after,
    createdUserId
  );
}

module.exports = {
  router,
  validate,
};
