//Determine to use test database in repository/connection.js
process.env.NODE_ENV = "test";

const testModel = require("../../../models/order.js");
const server = require("../../../app.js");
const chai = require("chai");
const chaiHttp = require("chai-http");
var expect = require("chai").expect;
var ObjectID = require("mongodb").ObjectID;
chai.use(chaiHttp);

describe("/GET order", () => {
  it("it should GET all the orders", (done) => {
    //Dummy data then execute query
    let testData = {
      customerName: "TEST ORDER",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 100000,
      requestUserId: new ObjectID(),
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("result");
        response.should.have.property("result").equal("OK");

        // query data on fake database
        chai
          .request(server)
          .get(process.env.API_ORDER_URL)
          .end((err, res) => {
            res.should.have.status(200);
            const length = JSON.parse(res.text).totalCount;
            expect(length).to.equal(1);
            done();
          });
      });
  });
});

describe("/GET > checkstatus", () => {
  it("it should not response status with order number not exist", (done) => {
    const orderNumber = "dummy";
    // query data on fake database
    chai
      .request(server)
      .get(process.env.API_ORDER_URL + "/checkstatus/" + orderNumber)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("result");
        response.should.have.property("result").equal("ERROR");
        response.should.have.property("message").equal(process.env.MSG_NOT_EXIST);
        done();
      });
  });
});

describe("/GET > checkstatus", () => {
  it("it should response order with status Created", (done) => {
    const orderNumber = "ORDER-00001";
    //Dummy data then execute query
    let testData = {
      customerName: "TEST ORDER",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 100000,
      requestUserId: new ObjectID(),
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("result");
        response.should.have.property("result").equal("OK");

        // query data on fake database
        chai
          .request(server)
          .get(process.env.API_ORDER_URL + "/checkstatus/" + orderNumber)
          .end((err, res) => {
            res.should.have.status(200);
            const status = JSON.parse(res.text).status;
            expect(status).to.equal("Created");
            done();
          });
      });
  });
});
