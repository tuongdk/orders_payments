//Determine to use test database in repository/connection.js
process.env.NODE_ENV = "test";

const testModel = require("../../../models/order.js");
const server = require("../../../app.js");
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
var ObjectID = require("mongodb").ObjectID;
chai.use(chaiHttp);

describe("/POST order", () => {
  it("it should not create a order without customerName", (done) => {
    let testData = {
      customerName: "",
      shipToName: "",
      shipToAddress: "",
      totalAmount: 0,
      requestUserId: undefined,
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("message");
        response.should.have
          .property("message")
          .equal("Customer name is required.");
        done();
      });
  });

  it("it should not create a order without shipToName", (done) => {
    let testData = {
      customerName: "TEST customerName",
      shipToName: "",
      shipToAddress: "",
      totalAmount: 0,
      requestUserId: undefined,
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("message");
        response.should.have
          .property("message")
          .equal("Ship to name is required.");
        done();
      });
  });

  it("it should not create a order without shipToAddress", (done) => {
    let testData = {
      customerName: "TEST customerName",
      shipToName: "TEST shipToName",
      shipToAddress: "",
      totalAmount: 0,
      requestUserId: undefined,
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("message");
        response.should.have
          .property("message")
          .equal("Ship to address is required.");
        done();
      });
  });

  it("it should not create a order without totalAmount", (done) => {
    let testData = {
      customerName: "TEST customerName",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 0,
      requestUserId: undefined,
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("message");
        response.should.have
          .property("message")
          .equal("Total amount must be greater than zero.");
        done();
      });
  });

  it("it should not create a order without requestUserId", (done) => {
    let testData = {
      customerName: "TEST customerName",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 100000,
      requestUserId: undefined,
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("message");
        response.should.have
          .property("message")
          .equal("Missing parameter requestUserId.");
        done();
      });
  });

  it("it should create a order ", (done) => {
    beforeEach((done) => {
      testModel.deleteMany({ customerName: "TEST ORDER" }, (err) => {
        done();
      });
    });

    let testData = {
      customerName: "TEST ORDER",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 100000,
      requestUserId: new ObjectID(),
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("result");
        response.should.have.property("result").equal("OK");
        done();
      });
  });
});