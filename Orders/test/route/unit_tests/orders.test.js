//Determine to use test database in repository/connection.js
process.env.NODE_ENV = "test";

const testModel = require("../../../models/order.js");
const server = require("../../../app.js");
const chai = require("chai");
const chaiHttp = require("chai-http");
var expect = require("chai").expect;
var ObjectID = require("mongodb").ObjectID;
var assert = require("assert");
const orderRoute = require("../../../routes/order");
chai.use(chaiHttp);

describe("Unit tests method validate() in order", function () {
  it("should return error with invalid customer name", function () {
    const resultValidation = orderRoute.validate({
      customerName: "",
      shipToName: "",
      shipToAddress: "",
      totalAmount: 0,
      requestUserId: undefined,
    });
    assert.equal(resultValidation.result, "ERROR");
    assert.equal(resultValidation.message, "Customer name is required.");
  });

  it("should return error with invalid ship to name", function () {
    const resultValidation = orderRoute.validate({
      customerName: "TEST customerName",
      shipToName: "",
      shipToAddress: "",
      totalAmount: 0,
      requestUserId: undefined,
    });
    assert.equal(resultValidation.result, "ERROR");
    assert.equal(resultValidation.message, "Ship to name is required.");
  });

  it("should return error with invalid ship to address", function () {
    const resultValidation = orderRoute.validate({
      customerName: "TEST customerName",
      shipToName: "TEST shipToName",
      shipToAddress: "",
      totalAmount: 0,
      requestUserId: undefined,
    });
    assert.equal(resultValidation.result, "ERROR");
    assert.equal(resultValidation.message, "Ship to address is required.");
  });

  it("should return error with invalid total amount", function () {
    const resultValidation = orderRoute.validate({
      customerName: "TEST customerName",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 0,
      requestUserId: undefined,
    });
    assert.equal(resultValidation.result, "ERROR");
    assert.equal(
      resultValidation.message,
      "Total amount must be greater than zero."
    );
  });

  it("should return error with invalid requestUserId", function () {
    const resultValidation = orderRoute.validate({
      customerName: "TEST customerName",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 100000,
      requestUserId: undefined,
    });
    assert.equal(resultValidation.result, "ERROR");
    assert.equal(resultValidation.message, "Missing parameter requestUserId.");
  });

  it("should return success with valid parameters", function () {
    const resultValidation = orderRoute.validate({
      customerName: "TEST customerName",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 100000,
      requestUserId: new ObjectID(),
    });
    assert.equal(resultValidation.result, "OK");
  });
});
