//Determine to use test database in repository/connection.js
process.env.NODE_ENV = "test";

const testModel = require("../../../models/order.js");
const server = require("../../../app.js");
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
var ObjectID = require("mongodb").ObjectID;
var expect = require("chai").expect;
chai.use(chaiHttp);

describe("/PATCH order > cancelorder", () => {
  it("it should response error with order number does not exist", (done) => {
    const orderNumber = "dummy";
    let testData = {
      requestUserId: new ObjectID(),
    };
    chai
      .request(server)
      .patch(process.env.API_ORDER_URL + "/cancelorder/" + orderNumber)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("result");
        response.should.have.property("result").equal("ERROR");
        response.should.have.property("message");
        response.should.have
          .property("message")
          .equal(process.env.MSG_NOT_EXIST_UPDATE);
        done();
      });
  });

  it("it should success cancel with valid order number", (done) => {
    const orderNumber = "ORDER-00001";
    //Dummy data then execute query
    let testData = {
      customerName: "TEST ORDER",
      shipToName: "TEST shipToName",
      shipToAddress: "TEST shipToAddress",
      totalAmount: 100000,
      requestUserId: new ObjectID(),
    };
    chai
      .request(server)
      .post(process.env.API_ORDER_URL)
      .send(testData)
      .end((err, res) => {
        res.should.have.status(200);
        const response = JSON.parse(res.text);
        response.should.have.property("result");
        response.should.have.property("result").equal("OK");

        // query data on fake database
        let testPatchData = {
          requestUserId: new ObjectID(),
        };
        chai
          .request(server)
          .patch(process.env.API_ORDER_URL + "/cancelorder/" + orderNumber)
          .send(testPatchData)
          .end((err, res) => {
            res.should.have.status(200);
            response.should.have.property("result");
            response.should.have.property("result").equal("OK");
            response.should.have.property("message");
            response.should.have
              .property("message")
              .equal(process.env.MSG_CANCEL_SUCCESS);
            done();
          });
        done();
      });
  });
});
