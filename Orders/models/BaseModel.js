const mongoose = require("mongoose");

module.exports = function (paths) {
  var schema = new mongoose.Schema({
    createdUserId: { type: mongoose.Types.ObjectId, required: false, index: false },
    modifiedUserId: { type: mongoose.Types.ObjectId, required: false, index: false },
    modifiedDate: { type: Date, required: false, index: false },
    createdDate: { type: Date, required: false, index: false },
  });
  schema.add(paths);
  return schema;
};