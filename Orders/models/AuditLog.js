const mongoose = require("mongoose");
const { logConnection } = require("../repository/connectionaudit");

const schema = mongoose.Schema({
  processorId: {
    type: String,
  },
  objectId: {
    type: String,
  },
  message: {
    type: String,
  },
  action: {
    type: String,
  },
  type: {
    type: String,
    require: true,
  },
  before: {
    type: String,
  },
  after: {
    type: String,
  },
  createdUserId: {
    type: String,
  },
  createdDate: {
    type: Date,
  },
});

module.exports = logConnection.model("setelauditlogs", schema);