const mongoose = require("mongoose");
var base = require("./basemodel");
var Float = require("mongoose-float").loadType(mongoose);

const schema = new base({
  status: {
    type: String,
    require: true,
    default: "Created",
  },
  orderNumber: {
    type: String,
    require: true,
  },
  purchasedDate: {
    type: Date,
    default: new Date(),
  },
  customerName: {
    type: String,
    require: true,
  },
  shipToName: {
    type: String,
    require: true,
  },
  shipToAddress: {
    type: String,
    require: true,
  },
  totalAmount: {
    type: Float,
    default: 0,
    require: true,
  },
});

module.exports = mongoose.model("setelorders", schema);
