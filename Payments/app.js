const express = require("express");
const app = express();
const bodyParser = require("body-parser");
require("dotenv/config");

app.use(bodyParser.json({ limit: "30000kb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30000kb", extended: true }));

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader("Access-Control-Allow-Methods", "POST");
  next();
});

const paymentRoute = require("./routes/Payment");
app.use("/payment", paymentRoute);

app.listen(process.env.PORT);
