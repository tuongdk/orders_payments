const express = require("express");
const router = express.Router();

router.post("/", async (req, res) => {
  try {
      const token = req.body.token;
      const payment = req.body.payment;
      
      //Simple validate payment
      let errorMessage = "";
      if(token === undefined || token !== process.env.PAYMENT_TOKEN)
      {
        errorMessage = process.env.MSG_ERROR_PAYMENT_TOKEN;
      }
      if(errorMessage === "" && (payment === null || payment === undefined)){
        errorMessage = process.env.MSG_ERROR_PAYMENT_NULL;
      }
      if(errorMessage === "" && payment.amount <= 0){
        errorMessage = process.env.MSG_ERROR_PAYMENT_AMOUNT_INVALID;
      }
      if(errorMessage === "" && (payment.shipToName === undefined || payment.shipToName === null || payment.shipToName === "")){
        errorMessage = process.env.MSG_ERROR_SHIP_NAME_INVALID;
      }
      if(errorMessage === "" && (payment.shipToAddress === undefined || payment.shipToAddress === null || payment.shipToAddress === "")){
        errorMessage = process.env.MSG_ERROR_SHIP_ADDRESS_INVALID;
      }
      
      if(errorMessage !== ""){
        return res.json({
          result: 'ERROR',
          message: errorMessage,
        })
      }
      //random result
      const responseCodes = [process.env.STATUS_DECLINED, process.env.STATUS_CONFIRMED];
      var randomCode = responseCodes[Math.floor(Math.random() * responseCodes.length)];
      res.json({ result: "OK", responseCode: randomCode });
  } catch (err) {
    res.json({ result: "ERROR", message: err.message });
  }
});

module.exports = router;